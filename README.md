### What is this repository for? ###

* A couple of scripts to help find iOS simulator folders such as the documents, and app folders.
* Version 0.1

### How do I get set up? ###

* Clone the repository with ` git clone https://bitbucket.org/James_Snook/simulatorfolders` or download with this link https://bitbucket.org/James_Snook/simulatorfolders/get/a1c26c946dd9.zip
* Go to the repository folder.
* To run the simFolders.sh script type `./simFolders.sh`
* To run the simulatorAppFolders.sh script type `simulatorAppFolders.sh`
* Alternatively put these scripts somewhere in your path. Then you can run them from anywhere just by typing their name.

####simFolders.sh

Creates a more humanly named clone of the simulator folders, symlinking to the Documents and App folder at the end.

simFolders.sh [-f?]

-f Is an optional argument which causes the script to flattern out folders wich only contain 1 folder. Instead of a folder containing a folder there will only be one folder with it's name combined. i.e. test/foo/bar becomes test_foo/bar if test only contains foo.

-? Prints this help dialog

WARNING: This will delete and replace any folder called /Users/james/iOS_SIMULATORS you can change the folder by editing the SIM_CLONE_FOLDER in the script.

####simulatorAppFolders.sh

simulatorAppFolders.sh (-s <name> | -d <device-type>) [-?] [-i <iOS version>] [-a <app-name>]

-? show help, and prints simulators currently installed

-s <simulator name> the name of the device as it appears in Xcode's device list. Prints the folder of this simulator without -i will choose the first matching folder

-d <simulator device type> the device type as it appears in the simulator plist file. Prints the folder of this simulator without -i will choose the first matching folder

-i <iOS version> finds the folder matching the iOS version as well as the simulator name

-p Used with -s to print the installed apps on a simulator

-a <application name> Used with -s to print the application and data folder of the application

###Additional

You can find out more information about these scripts on my [website](http://www.twistedape.me.uk/blog/blog/2014/09/15/xcode-6-simulator-folders/) my contact details are also there.