#! /bin/bash

SIM_DEVICES_FOLDER=~/Library/Developer/CoreSimulator/Devices
SIM_RUNTIME_PRFIX=com.apple.CoreSimulator.SimRuntime

SIM_CLONE_FOLDER=~/iOS_SIMULATORS

me=`basename $0`

function usage () {
  echo "Creates a more humanly named clone of the simulator folders, symlinking to the Documents and App folder at the end."
  echo
  echo "${me} [-f?]"
  echo
  echo "-f Is an optional argument which causes the script to flattern out folders wich only contain 1 folder. Instead of a folder containing a folder there will only be one folder with it's name combined. i.e. test/foo/bar becomes test_foo/bar if test only contains foo."
  echo
  echo "-? Prints this help dialog"
  echo
  echo "WARNING: This will delete and replace any folder called ${SIM_CLONE_FOLDER} you can change the folder by editing the SIM_CLONE_FOLDER in the script."

 exit 1
}

OPTIND=1

FLATTERN=""

while getopts '?f' opt
do
  case "$opt" in
    f) FLATTERN=falttern
    ;;
    ?) usage
    ;;
  esac
done

if [ -d ${SIM_CLONE_FOLDER} ]; then
  rm -rf ${SIM_CLONE_FOLDER}
fi

mkdir ${SIM_CLONE_FOLDER}

cd ${SIM_DEVICES_FOLDER}

for dir in *
do
  simName=$(/usr/libexec/PlistBuddy -c "print :name" ${dir}/device.plist)
  simRuntime=$(/usr/libexec/PlistBuddy -c "print :runtime" ${dir}/device.plist)
  simDeviceType=$(/usr/libexec/PlistBuddy -c "print :deviceType" ${dir}/device.plist)

  simDeviceType="${simDeviceType##*.}"
  simRuntime="${simRuntime##*.}"

  CURRENT_FOLDER=${SIM_CLONE_FOLDER}
  cd "${CURRENT_FOLDER}"
  if [ ! -d "$simDeviceType" ]; then
    mkdir "${simDeviceType}"
  fi

  cd "${simDeviceType}"
  CURRENT_FOLDER=${CURRENT_FOLDER}/${simDeviceType}

  if [ ! -d "$simRuntime" ]; then
    mkdir "$simRuntime"
  fi

  cd "${simRuntime}"
  CURRENT_FOLDER=${CURRENT_FOLDER}/${simRuntime}
  
  if [ ! -d "$simName" ]; then
    mkdir "$simName"
  fi

  cd "${simName}"
  CURRENT_FOLDER=${CURRENT_FOLDER}/${simName}

  DEVICE_FOLDER="${SIM_DEVICES_FOLDER}/${dir}"
  DATA_FOLDER=${DEVICE_FOLDER}/data
  
  if [ ! -d "$DATA_FOLDER/Containers" ]; then
    # There is no Applications built for this device so we had
    # better clear up. We also do more clearing up later.
    cd ..
    rm -r "${simName}"
  else
    APPS=Apps
    mkdir ${APPS}
    cd ${APPS}
    CURRENT_FOLDER="${CURRENT_FOLDER}/${APPS}"

    APPLICATION_FOLDER=${DEVICE_FOLDER}/data/Applications

     if [ -d "${APPLICATION_FOLDER}" ]; then
      #We're in an iOS 7 device.
      for dir in "${APPLICATION_FOLDER}"/*
      do
        appName=`find "${dir}" -iname *.app`
        fileName=$(basename $appName)
        fileName="${fileName%.*}"
        ln -s "${dir}" "${fileName}"
      done
    else
      #We're in an iOS 8 device.
      CONTAINER_FOLDER=${DEVICE_FOLDER}/data/Containers
      for dir in ${CONTAINER_FOLDER}/Bundle/Application/*
      do
        for app in ${dir}/*.app
        do
          fileName=$(basename ${app})
          appName="${fileName%.*}"

          mkdir "${appName}"
          cd "${appName}"
          appFolder="${CURRENT_FOLDER}/${appName}"
          ln -s "${dir}" "${fileName}"
          
          #Finally we need to find the Documents folder and link this as well.
          BUNDLE_IDENTIFIER=$(/usr/libexec/PlistBuddy -c "print :CFBundleIdentifier" "${app}/Info.plist")
          APPLICATION_DATA_FOLDER=${CONTAINER_FOLDER}/Data/Application
          for dir in ${APPLICATION_DATA_FOLDER}/*
          do
            IDENTIFIER=$(/usr/libexec/PlistBuddy -c "print :MCMMetadataIdentifier" ${dir}/.com.apple.mobile_container_manager.metadata.plist)
              if [ "${IDENTIFIER}" == "${BUNDLE_IDENTIFIER}" ]; then
                  ln -s "${dir}" Data
                  break
              fi
          done
        done
      done
    fi
  fi

  cd ${SIM_DEVICES_FOLDER}
done

if [ "${FLATTERN}" == "" ]; then
  exit 0
fi

cd "${SIM_CLONE_FOLDER}"

function reduceFolders () {
  BASENAME=$(basename "$1")
  if [ "${BASENAME}" == "Applications" ]
  then
    DIR_COUNT=$(find "$1" -maxdepth 0 -type d | wc -l)
    if [ ! ${DIR_COUNT} ]; then
      rm -rf "$1"
    fi
    return
  fi

  DIR_COUNT=$(find "${1}/" -maxdepth 1 -type d | wc -l)
  DIR_COUNT=$((${DIR_COUNT} - 1))
  
  if [ $DIR_COUNT != 0 ]; then
    for dir in ${1}/*
    do 
      if [ -d "${dir}" ]; then
        reduceFolders "${dir}"
      fi
    done
  fi

  DIR_COUNT=$(find "${1}/" -maxdepth 1 -type d | wc -l)
  DIR_COUNT=$((${DIR_COUNT} - 1))

  if [ ${DIR_COUNT} == 0 ]; then
    rm -rf "$1"
    return 
  fi

  if [ ${DIR_COUNT} == 1 ]; then
    for subDir in "${1}"/*
    do
      #if there is only one subfolder then we don't really need a folder to contain it.
      cp -a "${subDir}/" "${1}"
      rm -rf "${subDir}"
      BASENAME=$(basename "${subDir}")

      if [[ ${BASENAME} = iOS* ]]; then
        mv "${1}" "${BASENAME}"
      else 
        mv "${1}" "${1}_${BASENAME}"
      fi
    done  
  fi
}

reduceFolders "${SIM_CLONE_FOLDER}"
